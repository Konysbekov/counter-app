package com.example.counterapp

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder

class CounterService : Service() {

    private var count = 0

    fun increaseCount() {
        count += 1
    }

    fun decreaseCount() {
        count -= 1
    }

    fun getCount(): Int {
        return count
    }

    inner class CounterBinder : Binder() {
        fun getService(): CounterService = this@CounterService
    }

    override fun onBind(intent: Intent): IBinder {
        return CounterBinder()
    }
}
