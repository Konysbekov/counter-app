package com.example.counterapp

import android.content.Context
import android.widget.Toast
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private lateinit var counterService: CounterService
    private var isServiceBound = false

    private fun displayToast(message: String) {
        applicationContext?.let {
            runOnUiThread {
                Toast.makeText(it, message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as CounterService.CounterBinder
            counterService = binder.getService()
            isServiceBound = true
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            isServiceBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val incrementButton = findViewById<Button>(R.id.increment_button)
        val decrementButton = findViewById<Button>(R.id.decrement_button)
        val showCountButton = findViewById<Button>(R.id.show_count_button)

        incrementButton.setOnClickListener {
            if (isServiceBound) {
                counterService.increaseCount()
            }
        }

        decrementButton.setOnClickListener {
            if (isServiceBound) {
                counterService.decreaseCount()
            }
        }

        showCountButton.setOnClickListener {
            if (isServiceBound) {
                val currentCount = counterService.getCount()
                displayToast("Current count: $currentCount")
            } else {
                displayToast("Service is not bound")
            }
        }
    }

    override fun onStart() {
        super.onStart()
        Intent(this, CounterService::class.java).also { intent ->
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        if (isServiceBound) {
            unbindService(serviceConnection)
            isServiceBound = false
        }
    }
}
